import csv
import json
import matplotlib.pyplot as plt
from datetime import datetime

# Specify the paths to your JSON files
json_file_path1 = './source/wattage.json'
json_file_path2 = './source/cpuutil.json'

# Read the first JSON data from the first file
with open(json_file_path1, 'r') as file1:
    json_data1 = json.load(file1)

# Read the second JSON data from the second file
with open(json_file_path2, 'r') as file2:
    json_data2 = json.load(file2)

# Extract Time and Average from the first dataset
data1 = json_data1["PowerDetail"]
time_values1 = [datetime.fromisoformat(entry["Time"].replace("Z", "+00:00")) for entry in data1]
average_values1 = [entry["Average"] for entry in data1]

# Extract Time and MetricValue from the second dataset
data2 = json_data2["CPUUtil"]
time_values2 = [datetime.fromisoformat(entry["Timestamp"].replace("Z", "+00:00")) for entry in data2]
metric_values2 = [float(entry["MetricValue"]) for entry in data2]

# Write the data from each dataset to separate CSV files
csv_file1 = "./output/power_data.csv"
csv_file2 = "./output/cpu_util_data.csv"

with open(csv_file1, mode='w', newline='') as file1:
    writer1 = csv.writer(file1)
    writer1.writerow(["Time", "Average"])  # Write header row
    writer1.writerows(zip(time_values1, average_values1))  # Write data rows

with open(csv_file2, mode='w', newline='') as file2:
    writer2 = csv.writer(file2)
    writer2.writerow(["Time", "MetricValue"])  # Write header row
    writer2.writerows(zip(time_values2, metric_values2))  # Write data rows

print(f'Data from the first JSON file has been written to "{csv_file1}".')
print(f'Data from the second JSON file has been written to "{csv_file2}".')

# Create two subplots sharing the same X-axis
fig, ax1 = plt.subplots(figsize=(10, 5))
ax2 = ax1.twinx()

# Plot the PowerDetail data on the left Y-axis
ax1.plot(time_values1, average_values1, marker='o', linestyle='-', label="Power Average", color='blue')
ax1.set_xlabel('Time')
ax1.set_ylabel('Power Average', color='blue')
ax1.tick_params(axis='y', labelcolor='blue')
ax1.set_ylim(0, 600)

# Plot the CPU Usage data on the right Y-axis
ax2.plot(time_values2, metric_values2, marker='x', linestyle='-', label="CPU Utilization", color='red')
ax2.set_ylabel('CPU Utilization', color='red')
ax2.tick_params(axis='y', labelcolor='red')
ax2.set_ylim(0, 100)

# Title and legend
plt.title('Combined Data')
plt.legend(loc='upper left')

# Format x-axis labels as dates
plt.xticks(rotation=45)

# Save the plot as a PNG image
plot_image_file = "./output/combined_plot.png"
plt.savefig(plot_image_file)

# Show the plot
plt.tight_layout()
plt.show()

print(f'Combined plot has been saved as "{plot_image_file}".')
