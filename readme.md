# Metriken aus ILO auslesen

# Daten holen

1. ILO öffnen
2. "Power and Thermal" wählen
3. Oben "Power Meter" wählen
4. Developer Tools im Browser öffnen
5. "Netzwerk"
6. Auf "24h" stellen
7. Im "Power Meter" refresh drücken
8. Im Netzwerk den Request "https://172.16.224.91/redfish/v1/Chassis/1/Power/FastPowerMeter/..." suchen
9. Die response in die Datei ./source/wattage.json kopieren
10. "Performance -> Monitoring" öffnen
11. Auf "24h" stellen
12. Im Netzwerk den Request "/redfish/v1/Views/" suchen
13. Die response in die Datei ./source/cpuutil kopieren
14. Python script laufen lassen
15. Im Ordner output befinden sich die Daten und ein PNG mit dem Graph

## Anmerkungen
Der Datensatz für die Leistungsaufnahme hat andere Beobachtungszeiträume
als der Datensatz für die CPU Utilization, 24h ist der einzige gemeinsame 
Zeitraum. Mit dieser Einstellung passen die Daten sicher zusammen.
Mit anderen Zeiträumen muss man erst noch experimentieren.
